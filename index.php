<?php 
//Conexión a bbdd llamando al rchivo de conexión
require('includes/conexion.php');

//Llamamos a la conversion de coordenas utm a lat y long
require('includes/phpcoord.php');

//Llamamos a las librerias de /vendor/
require('vendor/autoload.php');

//Indicamos donde van a ir las plantillas de twig
$loader = new Twig_Loader_Filesystem('views/');

//Esto es para desarrollo
$twig = new Twig_Environment($loader);

//Esto es para produccion
//$twig = new Twig_Environment($loader, array('cache' => 'cache/'));

//Llamar a un controlador por defecto productosController.php
if(isset($_GET['c'])){
  $c=$_GET['c'];
}else{
  $c='productosController.php';
}

require('controllers/'.$c);

//Desconexión bbdd
$conexion->close();

?>