<?php  
//Fichero controllers/deportivosController.php


//El controlador, tiene que llamar al modelo
// de datos, y pasar los resultados a la vista
require('models/centroModel.php');
require('models/centrosModel.php');
$centros=new Centros();

if(isset($_GET['id'])){
	//Para conseguir un único centro
	$elcentro=$centros->dimeElemento($_GET['id']);
	echo $twig->render('centroView.html.twig', Array('elcentro'=>$elcentro));
}else{
	//Para conseguir un listado de todos los centros
	$loscentros=$centros->dimeElementos();
	echo $twig->render('centrosView.html.twig', Array('loscentros'=>$loscentros));
}
?>