<?php 
//Fichero controllers/categoriasController.php

//El controlador llama al modelo de datos y pasa los resultados a la vista
require('models/categoriaModel.php');
require('models/catalogoModel.php');

$catalogo=new Catalogo();

// Con el modelo de datos aquí, extraigo los datos que pasaré a la vista
if(isset($_GET['idCat'])){
	$categoria=$catalogo->dimeCategoria($_GET['idCat']);
	echo $twig->render('categoria.html.twig', Array('categoria'=>$categoria));
}else{
	$categorias=$catalogo->dimeCategorias();
	echo $twig->render('categorias.html.twig', Array('categorias'=>$categorias));
}

?>