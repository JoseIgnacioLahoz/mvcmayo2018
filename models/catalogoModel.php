<?php 
//Fichero models/catalogoModel.php

class Catalogo{
	public $elementos; //Será un vector de posts

	public function __construct(){
		$this->elementos=[]; // Le indico que será un vector vacio de momento
	}

	public function dimeCategorias(){
		global $conexion; //Hago alusión a la conexión global
		$sql="SELECT * FROM categorias ORDER BY idCat";
		$consulta=$conexion->query($sql);
		while($registro=$consulta->fetch_array()){
			$this->elementos[]=new Categoria($registro);
		}
		return $this->elementos; //Devuelve un array de posts

	}

	public function dimeCategoria($idCat){
		global $conexion; //Hago alusión a la conexión global
		$sql="SELECT * FROM categorias WHERE idCat=$idCat";
		$consulta=$conexion->query($sql);
		$registro=$consulta->fetch_array();
		$elemento=new Categoria($registro);
		return $elemento; //Devuelve un solo post

	}

} //Fin de la class Catalogo

?>