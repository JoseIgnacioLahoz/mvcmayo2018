<?php 
//Fichero models/categoriaModel.php

//Constructor de la clase Categoria
class Categoria{
	public $idCat;
	public $nombreCat;
	public $descripcionCat;


	public function __construct($registro){
		$this->idCat=$registro['idCat'];
		$this->nombreCat=$registro['nombreCat'];
		$this->descripcionCat=$registro['descripcionCat'];

	}
} //Fin de la clase Categoria

?>