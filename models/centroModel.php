<?php  
// Fichero models/centroModel.php
Class Centro{
	public $id;
	public $nombre;
	public $direccion;
	public $cp;
	public $localidad;
	public $longitud;
	public $latitud;

	public function __construct($elemento){
		$this->id=$elemento->id;
		$this->nombre=$elemento->title;
		$this->cp=$elemento->address->{'postal-code'};
		$this->direccion=$elemento->address->{'street-address'};
		@$this->localidad=$elemento->address->locality;
		@$this->longitud=$elemento->location->longitude;
		@$this->latitud=$elemento->location->latitude;
	}
}

?>